# Olá, <img src="https://raw.githubusercontent.com/kaueMarques/kaueMarques/master/hi.gif" width="30px" height="30px"> Bem vindo ao desafio tech-test-payment-api da Pottencial !

O objetivo deste projeto é construir uma API REST utilizando .Net Core. expondo as rotas com o swagger.
A API deve possuir 3 operações:
- Registrar venda: Recebe os dados do vendedor + itens vendidos. Registra venda com status "Aguardando pagamento";
- Buscar venda: Busca pelo Id da venda;
- Atualizar venda: Permite que seja atualizado o status da venda.🚀

# Orientações

<details>
  <summary><strong>Rodando no Docker</strong></summary><br />

  O Banco de dados foi criado para iniciar com o docker.
  
  Veja as orientações abaixo para iniciar a execução do projeto.

  > Rode o serviço com o comando `docker-compose up -d`.

  > Instale as dependências com `dotnet restore`.

---
  
  ✨ **Dica:** Para rodar o projeto desta forma, obrigatoriamente você deve ter o ambiente `.NET` na versão 6.0.0 ou superior instalado em seu computador.

---

  ## Após a instalação das dependências

  > Use os comandos `dotnet ef database update` para iniciar nosso banco de dados.
  - Ele inicializa o banco de dados desenvolvido com a ORM Entity Framework. 
  Consulte o arquivo `docker-compose` para verificar as entradas para conexão de sua interface de banco de dados SQLServer.

  > Inicie a aplicação com os comandos `dotnet watch run`.
---

  <br/>
</details>

# Funcionamento

✨ **Atenção** As telas a seguir servem para ilustrar o funcionamento do projeto
<br>

![Rotas](./public/Rotas.png)

## Vendedor
A primeira rota que devemos utilizar é a de criar vendedor, pois só é possível realizar uma venda se o Id que for passado na hora da venda for de um vendedor cadastrado. 
<br/>


![CriarVendedor](./public/CriarVendedor.png)

O vendedor só vai ser criado se o cpf que for passado não estiver cadastrado no banco de dados, por isso, temos essas duas imagens que representam as possíveis respostas ao tentar criar um novo vendedor.

![VendedorCriado](./public/VendedorCriado.png)

--- 

![JaTemVendedor](./public/JaTemVendedor.png)

A próxima rota serve para listar todos os vendedores cadastrados.
<br/>

![TodosVendedores](./public/TodosVendedores.png)

Por fim podemos encontrar um vendedor específico passando o Id do vendedor.

![VendedorId](./public/VendedorId.png)

## Vendas

Agora vamos para parte principal do projeto que é a parte de vendas. A primeira rota é a de criar uma venda nela você tem que passar um indentificador único, lembrando que se tentar cadastrar duas vendas com o mesmo identificador ele vai retornar um erro, além do indentificador temos que passar o nome e preço dos produtos que serão comprados e o Id do vendedor conforme a imagem abaixo.
<br/>

![CriarVenda](./public/CriarVenda.png)

A próxima rota serve para obtermos todas as vendas cadastradas.

![TodasVendas](./public/TodasVendas.png)

Também podemos obter uma venda específica passado o id desta venda.
![VendaPorId](./public/VendaPorId.png)

Por fim podemos atualizar o status do pedido respeitando a regra de negócio que é:
A atualização de status deve permitir somente as seguintes transições:
- De: Aguardando pagamento  Para: Pagamento Aprovado
- De: Aguardando pagamento   Para: Cancelada
- De: Pagamento Aprovado    Para: Enviado para Transportadora
- De: Pagamento Aprovado   Para: Cancelada
- De: Enviado para Transportador. Para: Entregue
![AtualizaStatusErrado](./public/AtualizaStatusErrado.png)
<br/>

![AtualizaStatus](./public/AtualizaStatus.png)

Por fim temos a rota que podemos obter todas as vendas de um vendedor passando o Id deste vendedor.
![VendasPeloIdVendedor](./public/VendasPeloIdVendedor.png)

<details>
  <summary><strong> Mande seu feedback sobre o projeto!</strong></summary><br />

Se estiver a vontade, clone o repositório e, execute, veja o deploy e ajude a melhorar este projeto! Seu feedback será super bem vindo!


</details>