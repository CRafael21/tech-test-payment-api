using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VendasAPI.Context;
using VendasAPI.Models;

namespace VendasAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly VendasContext _context;
        public VendedorController(VendasContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult CriarVendedor(Vendedor vendedor)
        {
            var existeVendedor = _context.Vendedores.FirstOrDefault(x => x.Cpf == vendedor.Cpf);
            
            if(existeVendedor != null)
            {
                return BadRequest("Este vendedor já foi cadastrado");
            }

            _context.Vendedores.Add(vendedor);
            _context.SaveChanges();

            return Ok(vendedor);
        }

        [HttpGet]
        public IActionResult TodosVendedores()
        {
            var vendedores = _context.Vendedores.ToList();
            return Ok(vendedores);
        }
        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var vendedor = _context.Vendedores.Find(id);
            if(vendedor == null)
            {
                return NotFound();
            }
            return Ok(vendedor);
        }
    }
}