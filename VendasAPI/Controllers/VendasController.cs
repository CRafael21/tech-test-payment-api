using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VendasAPI.Context;
using VendasAPI.Models;

namespace VendasAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendasController : ControllerBase
    {
        private readonly VendasContext _context;
        public VendasController(VendasContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult CriarVenda(Venda venda)
        {
            var vendedor = _context.Vendedores.Find(venda.VendedorId);
            var existeEsteIdentificador = _context.Vendas.FirstOrDefault(x => x.IdentificadorPedido == venda.IdentificadorPedido);
            
            if (venda.Data == DateTime.MinValue)
                return BadRequest(new { Erro = "A data da tarefa não pode ser vazia" });

            if(venda.Produto.Count() == 0)
            {
                return BadRequest("Deve ter pelo menos um produto");
            }

            if(existeEsteIdentificador != null)
            {
                return BadRequest("O identificador de pedido deve ser único");
            }

            if (vendedor == null)
            {
                return NotFound("Não foi encontrado vendedor com esse Id");
            }

            venda.Vendedor = vendedor;
            venda.Status = EnumStatus.Aguardando_Pagamento;

            _context.Vendas.Add(venda);
            _context.SaveChanges();

            return CreatedAtAction(nameof(PegarVendaPorId), new { id = venda.VendaId}, venda);
        }
        
        [HttpGet]
        public ActionResult<IEnumerable<Venda>> PegarTodasVendas()
        {
            var vendas = _context.Vendas.Include(p => p.Vendedor).Include(x => x.Produto).ToList();
            
            return Ok(vendas);
        }

        [HttpGet("{id}")]
        public IActionResult PegarVendaPorId(int id)
        {
            var venda = _context.Vendas.Include(v => v.Vendedor).Include(x => x.Produto).FirstOrDefault(v => v.VendaId == id);

            if (venda is null)
            {
                return NotFound("Não foi encontrado nenhuma venda com este ID.");
            }
            var vendedor = _context.Vendedores.Find(venda.VendedorId);

            var result = new
            {
                venda,
                vendedor
            };

            return Ok(result);
        }

        [HttpGet("vendedor/{id}")]
        public IActionResult PegarVendaPorvendedor(int id)
        {
            var venda = _context.Vendas.Include(v => v.Vendedor).Include(x => x.Produto).Where(v => v.VendedorId == id);

            if (venda is null)
            {
                return NotFound("Não foi encontrado nenhuma venda com este ID.");
            }

            return Ok(venda);
        }

        [HttpPut("status/{id}")]
        public IActionResult AtualizarVenda(int id, EnumStatus status)
        {
            var venda = _context.Vendas.Find(id);

            if (venda is null)
            {
                return NotFound("Não foi possível encontrar esta venda");
            }
            else
            {
                switch (venda.Status)
                {
                    case EnumStatus.Aguardando_Pagamento:
                        {
                            if ((status == EnumStatus.Pagamento_Aprovado) || (status == EnumStatus.Cancelada))
                            {
                                venda.Status = status;
                                _context.SaveChanges();
                                return Ok($"Status status alterado com sucesso");
                            }
                            else
                            {
                                return BadRequest("O status só pode ser alterado para Pagamento_Aprovado ou Candelado");
                            }
                        }
                    case EnumStatus.Pagamento_Aprovado:
                        {
                            if ((status == EnumStatus.Enviado_Transportadora) || (status == EnumStatus.Cancelada))
                            {
                                venda.Status = status;
                                _context.SaveChanges();
                                return Ok($"Status status alterado com sucesso");
                            }
                            else
                            {
                               return BadRequest("O status só pode ser alterado para Enviado_Transportadora ou Candelado");
                            }
                        }
                    case EnumStatus.Enviado_Transportadora:
                        {
                            if (status == EnumStatus.Entregue)
                            {
                                venda.Status = status;
                                _context.SaveChanges();
                                return Ok($"Status status alterado com sucesso");
                            }
                            else
                            {
                                return BadRequest("O status só pode ser alterado para Entregue");
                            }
                        }
                    default:
                        {
                            return BadRequest("Atualização de status não permitida");
                        }
                }
            }
        }
    }
}