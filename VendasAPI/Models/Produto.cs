using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VendasAPI.Models
{
    public class Produto
    {
        [JsonIgnore]
        public int ProdutoId { get; set; }
        public string Nome { get; set; }
        public decimal Preco { get; set; }

        [JsonIgnore]
        public int VendaId { get; set; }
    }
}