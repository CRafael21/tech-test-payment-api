using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VendasAPI.Models
{
    public class Vendedor
    {
        [Key]
        [JsonIgnore]
        public int VendedorId { get; set; }
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }

        [JsonIgnore]
        public ICollection<Venda>? Vendas { get; set; }
    }
}