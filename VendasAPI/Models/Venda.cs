using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VendasAPI.Models
{
    public class Venda
    {
        [JsonIgnore]
        public int VendaId { get; set; }
        public DateTime Data { get; set; }
        public string IdentificadorPedido { get; set; }
        public EnumStatus Status { get; set; }
        
        public ICollection<Produto> Produto { get; set; }
    
        public int VendedorId { get; set; }
        [JsonIgnore]
        public Vendedor? Vendedor { get; set; }

    }
}