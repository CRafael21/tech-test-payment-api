using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VendasAPI.Models
{
    public enum EnumStatus
    {
        Aguardando_Pagamento,
        Pagamento_Aprovado,
        Cancelada,
        Enviado_Transportadora,
        Entregue
    }
}