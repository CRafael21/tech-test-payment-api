using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VendasAPI.Models;

namespace VendasAPI.Context
{
    public class VendasContext : DbContext
    {
        public VendasContext(DbContextOptions<VendasContext> options) : base(options)
        {

        }
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Venda> Vendas { get; set; }

         protected override void OnModelCreating(ModelBuilder mb)
        {   
            mb.Entity<Venda>()
                .HasIndex(c => c.IdentificadorPedido)
                .IsUnique();

            mb.Entity<Vendedor>()
                .HasIndex(c => c.Cpf)
                .IsUnique();
        }
    }
}
